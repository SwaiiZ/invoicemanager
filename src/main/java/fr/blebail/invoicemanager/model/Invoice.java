package fr.blebail.invoicemanager.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Invoice extends Estimate{

    public Invoice(Company company, String client, double price, Date date, String description) {
        super(company, client, price, date, description);
        instances.add(this);
    }

    public Invoice(Estimate e){
        super(e.getCompany(), e.getClient(), e.getPrice(), e.getDate(), e.getDescription());
    }

}