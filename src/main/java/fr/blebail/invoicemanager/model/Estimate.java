package fr.blebail.invoicemanager.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Estimate implements Serializable {

    public static List<Estimate> instances = new ArrayList<>();
    private Company company;

    private String client;

    private double price;

    private Date date;

    private String description;

    public Estimate(Company company, String client, double price, Date date, String description) {
        this.company = company;
        this.client = client;
        this.price = price;
        this.date = date;
        this.description = description;
        instances.add(this);
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public String getClient() {
        return client;
    }

    public void setClient(String client) {
        this.client = client;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String serialize() {
        return this.getClass().getSimpleName() +"{" +
                "company=" + company +
                ", client='" + client + '\'' +
                ", price=" + price +
                ", date=" + date +
                ", description='" + description + '\'' +
                '}';
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName() + " : " + company.toString() + " - " + client + " " + price + "€ - " + date ;
    }
}