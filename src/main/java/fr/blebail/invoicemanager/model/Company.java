package fr.blebail.invoicemanager.model;

import java.util.ArrayList;
import java.util.List;

public class Company {


    public static List<Company> instances = new ArrayList<>();
    private String name;

    private String address;

    private String siret;

    private List<Invoice> invoiceList;

    private List<Estimate> estimateList;

    public Company(String name, String address, String siret) {
        this.name = name;
        this.address = address;
        this.siret = siret;
        this.invoiceList = new ArrayList<>();
        this.estimateList = new ArrayList<>();
        instances.add(this);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getSiret() {
        return siret;
    }

    public void setSiret(String siret) {
        this.siret = siret;
    }

    public List<Invoice> getInvoiceList() {
        return invoiceList;
    }

    public void setInvoiceList(List<Invoice> invoiceList) {
        this.invoiceList = invoiceList;
    }

    public void addInvoice(Invoice i){
        this.invoiceList.add(i);
    }

    public void removeInvoice(Invoice i){
        if(this.invoiceList.contains(i))this.invoiceList.remove(i);
    }

    public List<Estimate> getEstimateList() {
        return estimateList;
    }

    public void setEstimateList(List<Estimate> estimateList) {
        this.estimateList = estimateList;
    }

    public void addEstimate(Estimate e){
        this.estimateList.add(e);
    }

    public void removeEstimate(Estimate e){
        if(this.estimateList.contains(e))this.estimateList.remove(e);
    }

    public Invoice signEstimate(Estimate e){
        return new Invoice(e);
    }

    @Override
    public String toString() {
        return "Company{" +
                "name='" + name + '\'' +
                ", address='" + address + '\'' +
                ", siret='" + siret + '\'' +
                '}';
    }
}