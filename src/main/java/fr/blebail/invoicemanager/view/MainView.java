package fr.blebail.invoicemanager.view;

import fr.blebail.invoicemanager.model.Company;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class MainView extends Pane {

    private VBox vBox = new VBox();

    private HBox hBox = new HBox();

    private Button invoiceManager = new Button("Invoice Manager");
    private Button estimateManager = new Button("Estimate Manager");
    private Button companyManager = new Button("Company Manager");

    public MainView() {
        this.init();
        this.getChildren().add(this.vBox);
    }

    public void init(){
        this.hBox.getChildren().addAll(this.invoiceManager, this.estimateManager, this.companyManager);
        this.hBox.setAlignment(Pos.CENTER);
        this.vBox.getChildren().addAll( this.hBox);
        initButtons();
    }

    public void initButtons(){
        invoiceManager.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                Stage stage = (Stage) hBox.getScene().getWindow();
                stage.getScene().setRoot(new InvoiceManagerView());
            }
        });

        companyManager.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                Stage stage = (Stage) hBox.getScene().getWindow();
                stage.getScene().setRoot(new CompanyManagerView());
            }
        });

        estimateManager.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                Stage stage = (Stage) hBox.getScene().getWindow();
                stage.getScene().setRoot(new EstimateManagerView());
            }
        });
    }
}
