package fr.blebail.invoicemanager.view;

import fr.blebail.invoicemanager.model.Company;
import fr.blebail.invoicemanager.model.Estimate;
import fr.blebail.invoicemanager.model.Invoice;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.security.Timestamp;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;

public class EstimateView extends Pane {

    private VBox vBox = new VBox();

    //Compay
    private HBox companyHBox = new HBox();
    private Label companyLabel = new Label("Company :");
    private ComboBox companyCB =  new ComboBox();

    //Client
    private HBox clientHBox = new HBox();
    private Label clientLabel = new Label("Client :");
    private TextField clientName = new TextField();

    //Price
    private HBox priceHBox = new HBox();
    private Label priceLabel = new Label("Price :");
    private TextField price = new TextField();

    //Date
    private HBox dateHBox = new HBox();
    private Label dateLabel = new Label("Date");
    private DatePicker date = new DatePicker();

    //Description
    private HBox descriptionHBox = new HBox();
    private Label descriptionLabel = new Label("Description :");
    private TextArea description = new TextArea();

    private Button createInvoice = new Button("Create");
    private Button back = new Button("Back");

    public EstimateView(List<Company> companies) {
        this.init(companies);
    }

    public void init(List<Company> companies){
        initVBox();
        initDate();
        initCompany(companies);
        initClient();
        initPrice();
        initDescription();
        initCreate(companies);

        this.back.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                Stage stage = (Stage) vBox.getScene().getWindow();
                stage.getScene().setRoot(new EstimateManagerView());
            }
        });
        this.getChildren().add(this.vBox);
    }

    public void initVBox(){
        this.vBox.getChildren().addAll(this.back, this.dateHBox, this.companyHBox, this.clientHBox, this.priceHBox, this.descriptionHBox, this.createInvoice);
        this.vBox.setAlignment(Pos.CENTER);
        this.vBox.setSpacing(10);
    }

    public void initCompany(List<Company> companies){
        List<String> companyNames = companies.stream().map(Company::getName).toList();
        this.companyCB.getItems().addAll(companyNames);
        this.companyHBox.getChildren().addAll(this.companyLabel,this.companyCB);
        this.companyHBox.setAlignment(Pos.CENTER);
        this.companyHBox.setSpacing(10);
    }

    public void initClient(){
        this.clientHBox.getChildren().addAll(this.clientLabel, this.clientName);
        this.clientHBox.setAlignment(Pos.CENTER);
        this.clientHBox.setSpacing(10);
    }

    public void initPrice(){
        this.priceHBox.getChildren().addAll(this.priceLabel, this.price);
        this.priceHBox.setAlignment(Pos.CENTER);
        this.priceHBox.setSpacing(10);
    }

    public void initDate(){
        this.dateHBox.getChildren().addAll(this.dateLabel, this.date);
        this.dateHBox.setAlignment(Pos.CENTER);
        this.dateHBox.setSpacing(10);
    }

    public void initDescription(){
        this.descriptionHBox.getChildren().addAll(this.descriptionLabel, this.description);
        this.descriptionHBox.setAlignment(Pos.CENTER);
        this.descriptionHBox.setSpacing(10);
    }

    public void initCreate(List<Company> companies){
        this.createInvoice.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent actionEvent) {
                LocalDate localDate = date.getValue();
                Instant instant = Instant.from(localDate.atStartOfDay(ZoneId.systemDefault()));
                Date date = Date.from(instant);
                Estimate e = new Estimate(companies.get(companyCB.getSelectionModel().getSelectedIndex()), clientName.getText(),Double.parseDouble(price.getText()), date,description.getText() );
                createPDF(e.serialize());
                Stage stage = (Stage) vBox.getScene().getWindow();
                stage.getScene().setRoot(new EstimateManagerView());
            }
        });
    }

    public void createPDF(String data){
        FileWriter myWriter = null;
        long ts = new Date().getTime();
        try {

            myWriter = new FileWriter("Invoice"+ ts + ".txt");
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        try {
            myWriter.write(data);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        try {
            myWriter.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
