package fr.blebail.invoicemanager.view;

import fr.blebail.invoicemanager.model.Company;
import fr.blebail.invoicemanager.model.Invoice;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.util.List;

public class CompanyManagerView extends Pane {

    ListView<String> list = new ListView<String>();

    private VBox vBox = new VBox();
    private HBox hBox = new HBox();

    private Button create = new Button("Create Company");
    private Button back = new Button("Back");

    public CompanyManagerView(){
        this.hBox.getChildren().addAll(back, list, create);
        this.hBox.setSpacing(50);
        this.hBox.setAlignment(Pos.CENTER);
        create.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                Stage stage = (Stage) hBox.getScene().getWindow();
                stage.getScene().setRoot(new CompanyView());
            }
        });
        back.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                Stage stage = (Stage) hBox.getScene().getWindow();
                stage.getScene().setRoot(new MainView());
            }
        });
        initList();
        this.getChildren().add(hBox);
    }

    public void initList(){
        List<String> companies = Company.instances.stream().map(Company::toString).toList();
        this.list.getItems().addAll(companies);
    }


}
