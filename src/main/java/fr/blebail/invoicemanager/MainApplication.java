package fr.blebail.invoicemanager;

import fr.blebail.invoicemanager.model.Company;
import fr.blebail.invoicemanager.model.Invoice;
import fr.blebail.invoicemanager.view.InvoiceManagerView;
import fr.blebail.invoicemanager.view.InvoiceView;
import fr.blebail.invoicemanager.view.MainView;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

public class MainApplication extends Application {
    @Override
    public void start(Stage stage) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(MainApplication.class.getResource("hello-view.fxml"));
        // Init test instances
        Company c1 = new Company("Test", "test", "test");
        new Invoice(c1, "client", 18099.99, new Date(), "aaaa");

        // Get Instances of company
        MainView mainView = new MainView();
        Scene scene = new Scene(mainView, 600 ,400);
        stage.setTitle("Hello!");
        stage.setScene(scene);
        stage.show();
    }


    public static void main(String[] args) {
        launch();
    }
}