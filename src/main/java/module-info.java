module fr.blebail.invoicemanager {
    requires javafx.controls;
    requires javafx.fxml;


    opens fr.blebail.invoicemanager to javafx.fxml;
    exports fr.blebail.invoicemanager;
}